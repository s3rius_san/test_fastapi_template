from typing import List

from testkaftemp.settings import settings

MODELS_MODULES: List[str] = ["testkaftemp.db.models.dummy_model"]  # noqa: WPS407

TORTOISE_CONFIG = {  # noqa: WPS407
    "connections": {
        "default": str(settings.db_url),
    },
    "apps": {
        "models": {
            "models": MODELS_MODULES,
            "default_connection": "default",
        },
    },
}
