"""Package to interact with rabbitMQ."""
from testkaftemp.web.gql.rabbit.mutation import Mutation

__all__ = ["Mutation"]
