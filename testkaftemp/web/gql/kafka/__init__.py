"""Package to interact with kafka."""
from testkaftemp.web.gql.kafka.mutation import Mutation

__all__ = ["Mutation"]
