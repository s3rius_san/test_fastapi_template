"""Redis API."""
from testkaftemp.web.gql.redis.mutation import Mutation
from testkaftemp.web.gql.redis.query import Query

__all__ = ["Query", "Mutation"]
