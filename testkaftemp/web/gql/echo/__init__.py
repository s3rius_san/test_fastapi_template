"""Echo API."""
from testkaftemp.web.gql.echo.mutation import Mutation
from testkaftemp.web.gql.echo.query import Query

__all__ = ["Query", "Mutation"]
