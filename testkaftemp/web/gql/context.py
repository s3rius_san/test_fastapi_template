from aio_pika import Channel
from aio_pika.pool import Pool
from aiokafka import AIOKafkaProducer
from fastapi import Depends
from redis.asyncio import Redis
from strawberry.fastapi import BaseContext

from testkaftemp.services.kafka.dependencies import get_kafka_producer
from testkaftemp.services.rabbit.dependencies import get_rmq_channel_pool
from testkaftemp.services.redis.dependency import get_redis_connection


class Context(BaseContext):
    """Global graphql context."""

    def __init__(
        self,
        redis: Redis = Depends(get_redis_connection),
        rabbit: Pool[Channel] = Depends(get_rmq_channel_pool),
        kafka_producer: AIOKafkaProducer = Depends(get_kafka_producer),
    ) -> None:
        self.redis = redis
        self.rabbit = rabbit
        self.kafka_producer = kafka_producer
        pass  # noqa: WPS420


def get_context(context: Context = Depends(Context)) -> Context:
    """
    Get custom context.

    :param context: graphql context.
    :return: context
    """
    return context
