"""Package for dummy model."""

from testkaftemp.web.gql.dummy.mutation import Mutation
from testkaftemp.web.gql.dummy.query import Query

__all__ = ["Query", "Mutation"]
