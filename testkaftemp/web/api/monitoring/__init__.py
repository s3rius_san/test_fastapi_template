"""API for checking project status."""
from testkaftemp.web.api.monitoring.views import router

__all__ = ["router"]
