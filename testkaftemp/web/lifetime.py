from typing import Awaitable, Callable

from fastapi import FastAPI

from testkaftemp.services.kafka.lifetime import init_kafka, shutdown_kafka
from testkaftemp.services.rabbit.lifetime import init_rabbit, shutdown_rabbit
from testkaftemp.services.redis.lifetime import init_redis, shutdown_redis


def register_startup_event(
    app: FastAPI,
) -> Callable[[], Awaitable[None]]:  # pragma: no cover
    """
    Actions to run on application startup.

    This function uses fastAPI app to store data
    inthe state, such as db_engine.

    :param app: the fastAPI application.
    :return: function that actually performs actions.
    """

    @app.on_event("startup")
    async def _startup() -> None:  # noqa: WPS430
        init_redis(app)
        init_rabbit(app)
        await init_kafka(app)
        pass  # noqa: WPS420

    return _startup


def register_shutdown_event(
    app: FastAPI,
) -> Callable[[], Awaitable[None]]:  # pragma: no cover
    """
    Actions to run on application's shutdown.

    :param app: fastAPI application.
    :return: function that actually performs actions.
    """

    @app.on_event("shutdown")
    async def _shutdown() -> None:  # noqa: WPS430
        await shutdown_redis(app)
        await shutdown_rabbit(app)
        await shutdown_kafka(app)
        pass  # noqa: WPS420

    return _shutdown
